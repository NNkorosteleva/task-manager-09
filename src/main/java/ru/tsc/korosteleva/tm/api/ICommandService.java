package ru.tsc.korosteleva.tm.api;

import ru.tsc.korosteleva.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
