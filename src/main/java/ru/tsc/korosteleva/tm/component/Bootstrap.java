package ru.tsc.korosteleva.tm.component;

import ru.tsc.korosteleva.tm.api.ICommandController;
import ru.tsc.korosteleva.tm.api.ICommandRepository;
import ru.tsc.korosteleva.tm.api.ICommandService;
import ru.tsc.korosteleva.tm.constant.ArgumentConst;
import ru.tsc.korosteleva.tm.constant.TerminalConst;
import ru.tsc.korosteleva.tm.controller.CommandController;
import ru.tsc.korosteleva.tm.repository.CommandRepository;
import ru.tsc.korosteleva.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        if (processArgument(args)) close();
        showProcess();
    }

    public void showProcess() {
        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public void close() {
        System.exit(0);
    }

}
